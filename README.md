# OpenML dataset: seismic-bumps

https://www.openml.org/d/40878

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The data describe the problem of high energy (higher than 10^4 J) seismic bumps forecasting in a coal mine. Data come from two of longwalls located in a Polish coal mine.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40878) of an [OpenML dataset](https://www.openml.org/d/40878). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40878/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40878/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40878/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

